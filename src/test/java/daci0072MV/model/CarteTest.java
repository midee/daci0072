package daci0072MV.model;

import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CarteTest {
    Carte c;
    Carte c1;

    private List<String> autori1 = new ArrayList<String>(
            Arrays.asList("Mircea Eliade","Victor Ionescu"));
    private List<String> autori3 =new ArrayList<String>(
            Arrays.asList("Victor Hugo","Daniel Ray","Thomas Craig"));
    private List<String> cuvinteCheie = new ArrayList<String>(
            Arrays.asList("nuvela","roman"));

    //se apeleaza o singura data inaintea oricarui test
    @BeforeClass
    public static void setup(){
        System.out.println("Before any test");

    }

    @AfterClass
    public static void setupAfter(){
        System.out.println("After all tests");

    }

    //se apeleaza doar inaintea testului asupra caruia ii este adnotat
    @Before
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("povesti");
        c.setEditura("Litera");
        System.out.println("in BeforeTest");

    }

    @After
    public void tearDown() throws Exception {
        c = null;
        System.out.println("in AfterTest");
    }

    @Ignore //ignora testul asupra caruia este pus
    @Test
    public void getTitlu() {
        assertEquals("titlu = povesti", "povesti",c.getTitlu());
        // assertTrue("povesti", c.getTitlu());
    }

    @Test
    public void setTitlu() {
        c.setTitlu("fabule");
        assertEquals("fabule",c.getTitlu());
        // assertTrue("povesti", c.getTitlu());
    }


    @Test(timeout = 100)// varianta cu 100 si 20 trece si cea cu 10 si cu 200 nu trece
    public void getEditura() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
            assertEquals(c.getEditura(), "Litera");
        }
    }


    // testul trece daca se arunca exceptia. trebuie sa simulam un comportament care sa arunce exceptia
    //pentru un an mai mic decat 1900 testul trece deoarece merge in exceptie (se arunca exceptie)

    @Test(expected =Exception.class)
    public void setAnAparitie() throws Exception{
        c.setAnAparitie("1899");
    }

    @Test(timeout =200)
    public void getAutori() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
            List<String> autori2 = new ArrayList<String>(
                    Arrays.asList("Mircea Eliade", "Victor Ionescu"));
            assertTrue(autori1.equals(autori2));
        }
    }

    @Test
    public void deleteAutor(){
        assertEquals(autori3.size(),3);
        autori3.remove(1);
        assertEquals(autori3.size(),2);

    }

    @Test
    public void adaugaCuvantCheie(){
          assertEquals(cuvinteCheie.size(),2);
          cuvinteCheie.add(1,"povesti");
          assertEquals(cuvinteCheie.size(),3);
          assertEquals(cuvinteCheie.get(1),"povesti");
    }
     @Ignore
    @Test
    public void deleteCuvantCheie(){
        assertEquals(cuvinteCheie.size(),2);
        cuvinteCheie.remove(0);
        assertEquals(cuvinteCheie.size(),1);
    }
}